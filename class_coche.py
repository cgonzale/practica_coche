import unittest

class Coche:
    def __init__(self, color, marca, modelo, matricula, velocidad=0):
        self.color = color
        self.marca = marca
        self.modelo = modelo
        self.matricula = matricula
        self.velocidad = velocidad

    def acelerar(self, incremento):
        self.velocidad += incremento

    def frenar(self, decremento):
        if self.velocidad - decremento >= 0:
            self.velocidad -= decremento
        else:
            self.velocidad = 0

