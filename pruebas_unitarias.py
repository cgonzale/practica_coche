import unittest
from class_coche import Coche

class TestCoche(unittest.TestCase):
    def setUp(self):
        self.coche = Coche(color="Rojo", marca="Ford", modelo="Fiesta", matricula="ABC1234")

    def test_constructor(self):
        self.assertEqual(self.coche.velocidad, 0)

    def test_acelerar(self):
        self.coche.acelerar(20)
        self.assertEqual(self.coche.velocidad, 20)

    def test_frenar(self):
        self.coche.acelerar(30)
        self.coche.frenar(10)
        self.assertEqual(self.coche.velocidad, 20)

    def test_frenar_hasta_detener(self):
        self.coche.acelerar(20)
        self.coche.frenar(30)
        self.assertEqual(self.coche.velocidad, 0)

if __name__ == '__main__':
    unittest.main()